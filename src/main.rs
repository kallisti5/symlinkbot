
extern crate natord;

use std::process;
use std::env;
use std::path::{Path,PathBuf};
use std::fs;
use std::error::Error;
use std::clone::Clone;
use std::os::unix;

use std::{thread, time};

#[derive(Clone, Debug)]
struct Repo {
    path: PathBuf,
    version: String,
}

/// Validate a repo is "ready" to be symlinked
fn repo_ready(path: PathBuf) -> bool {
    let repo_file = path.join("repo");
    let repo_info = path.join("repo.info");
    let repo_sig = path.join("repo.sig");
    if !repo_file.exists() {
        println!("Repo missing file: {}", repo_file.display());
        return false;
    } else if !repo_info.exists() {
        println!("Repo missing file: {}", repo_info.display());
        return false;
    } else if !repo_sig.exists() {
        println!("Repo missing file: {}", repo_sig.display());
        return false;
    }
    return true;
}

/// Locate the latest repos for each architecture
fn find_currents(prefix: PathBuf) -> Result<Vec<PathBuf>, Box<Error>> {
    let mut results: Vec<PathBuf> = Vec::new();
    let architectures = fs::read_dir(prefix)?;

    for arch in architectures {
        let mut repo_inventory: Vec<Repo> = Vec::new();

        let arch_path = arch?.path();
        if !arch_path.is_dir() {
            continue;
        }
        let repositories = fs::read_dir(&arch_path)?;
        for repo in repositories {
            let repo_path = repo?.path();
            if !repo_path.is_dir() {
                continue;
            }
            let repo_name = match repo_path.file_name() {
                Some(s) => s,
                None => continue,
            };
            let repo_string = match repo_name.to_str() {
                Some(s) => s,
                None => continue,
            };
            if repo_string == "current" || !repo_string.contains("hrev") {
                continue;
            }
            repo_inventory.push(Repo { path: repo_path.clone(), version: repo_string.to_string()});
        }
        if repo_inventory.is_empty() {
            continue
        }
        repo_inventory.sort_by(|a, b| natord::compare(&b.version, &a.version));
        results.push(repo_inventory.first().unwrap().path.clone());
    }
    return Ok(results);
}


/// Run through latest repos and create symlinks if ready
fn symlink_currents(currents: Vec<PathBuf>) -> Result<u32, Box<Error>> {
    let mut counter = 0;
    for current in currents {
        let parent = match current.parent() {
            Some(p) => p,
            None => {
                println!("Unable to symlink {}", current.display());
                continue
            }
        };
        let symlink_path = parent.join("current");
        let repo_path = Path::new(&current).strip_prefix(&parent)?;

        if !repo_ready(current.to_path_buf()) {
            println!("Repo at {} isn't ready yet!", current.display());
            continue
        }

        let link = fs::read_link(&symlink_path);
        if link.is_ok() {
            if link.unwrap() == repo_path {
                println!("Symlink {} already up to date!", symlink_path.display());
                continue
            }
            fs::remove_file(&symlink_path)?;
        }

        match unix::fs::symlink(&repo_path, &symlink_path) {
            Ok(_) => {
                println!("Symlink {} to {}...", symlink_path.display(), repo_path.display());
                counter += 1;
                },
            Err(e) => {
                println!("Symlink {} to {} failed: {}", symlink_path.display(), repo_path.display(), e);
            }
        }
    }

    return Ok(counter);
}

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: {} <prefix>", args[0]);
        process::exit(1);
    }
    let prefix = Path::new(&args[1]);
    if !prefix.exists() {
        println!("Error: Provided prefix '{}' doesn't exist!", args[1]);
        process::exit(1);
    }

    // Main process loop
    let nap_time = time::Duration::from_secs(60);
    loop {
        let currents = match find_currents(prefix.to_path_buf()) {
            Ok(c) => c,
            Err(e) => {
                println!("Error: {}", e);
                process::exit(1);
            }
        };
        let count = match symlink_currents(currents) {
            Ok(c) => c,
            Err(e) => {
                println!("Error: {}", e);
                process::exit(1);
            }
        };
        println!("Symlinked {} currents!", count);
        thread::sleep(nap_time);
    }
}
